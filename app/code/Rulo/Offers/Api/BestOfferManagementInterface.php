<?php

namespace Rulo\Offers\Api;

interface BestOfferManagementInterface
{
    /**
     * Return a Best Offer product.
     *
     * @param string $sku
     * @return mixed
     */
    public function getBestOffer(string $sku);
}
