<?php

namespace Rulo\Offers\Api;

use Rulo\Offers\Api\Data\RegisterSaleProductInterface;

interface RegisterSaleProductManagementInterface
{
    /**
     * @param RegisterSaleProductInterface $registerSaleProduct
     * @return mixed
     */
    public function registerSale(RegisterSaleProductInterface $registerSaleProduct);
}
