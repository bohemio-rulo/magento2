<?php

namespace Rulo\Offers\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface RegisterSaleProductInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const SKU = 'sku';

    const QTY = 'qty';

    const EMAIL = 'email';

    const INCREMENT_ID = 'increment_id';

    const CREATED_AT = 'created_at';
    /**#@-*/

    /**
     * Register Sale Product id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Register Sale Product id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Register Sale Product sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Set product sku
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Register Sale Product qty
     *
     * @return int|null
     */
    public function getQty();

    /**
     * Set Register Sale Product qty
     *
     * @param int $qty
     * @return $this
     */
    public function setQty($qty);

    /**
     * Register Sale Product email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Set Register Sale Product email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    /**
     * Register Sale Product increment_id
     *
     * @return string|null
     */
    public function getIncrementId();

    /**
     * Set Register Sale Product increment_id
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId);

    /**
     * Register Sale Product created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Register Sale Product created date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);
}
