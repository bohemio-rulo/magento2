<?php

namespace Rulo\Offers\Observer\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Rulo\Offers\Api\RegisterSaleProductManagementInterface;
use Rulo\Offers\Api\Data\RegisterSaleProductInterface;

class RegisterSaleProduct implements ObserverInterface
{
    /**
     * @var RegisterSaleProductInterface
     */
    protected $registerSaleProduct;

    /**
     * @var RegisterSaleProductManagementInterface
     */
    protected $registerSaleProductManagement;

    /**
     * @param RegisterSaleProductInterface $registerSaleProduct
     * @param RegisterSaleProductManagementInterface $registerSaleProductManagement
     */
    public function __construct(
        RegisterSaleProductInterface $registerSaleProduct,
        RegisterSaleProductManagementInterface $registerSaleProductManagement
    ) {
        $this->registerSaleProduct = $registerSaleProduct;
        $this->registerSaleProductManagement = $registerSaleProductManagement;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var \MAgento\Sales\Model\Order $order */
            $order = $observer->getOrder();
            foreach ($order->getAllVisibleItems() as $item) {
                $data = [
                    "sku" => $item->getSku(),
                    "qty" => $item->getQtyOrdered(),
                    "email" => $order->getCustomerEmail(),
                    "increment_id" => $order->getIncrementId(),
                    "store_id" => $order->getStoreId()
                ];

                $registerProduct = $this->registerSaleProduct;
                $registerProduct->setData($data);
                $this->registerSaleProductManagement->registerSale($registerProduct);
            }
        } catch (\Exception $e) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/exception-register-sale-product.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);

            $logger->info($e->getMessage());
        }
    }
}
