define([
    "jquery",
    "mage/url",
    "jquery/ui",
    "mage/validation"
], function($, urlBuild) {
    "use strict";

    $(document).ready(function(){
        $('#offer-form').click(function () {
            if ($(this).valid()) {
                $.ajax({
                    context: '#content-offer',
                    url: $(this).attr('action'),
                    type: "POST",
                    showLoader: true,
                    dataType: 'json',
                    data: $('#offer-form').serialize(),
                }).done(function (data) {
                    document.getElementById('offer-form').reset();
                    $('#offer-content-list').html(data.output);
                    return false;
                });
            }

            return false;
        });
    });

    function loadOfferList(config) {
        let ajaxUrl = config.ajaxUrl;

        $(document).ready(function(){
            $.ajax({
                context: '#offer-content-list',
                url: ajaxUrl,
                type: "POST",
                showLoader: true,
                dataType: 'json',
                data: $('#offer-form').serialize(),
            }).done(function (data) {
                $('#offer-content-list').html(data.output);
                return true;
            });
        });
    }

    return loadOfferList;
});
