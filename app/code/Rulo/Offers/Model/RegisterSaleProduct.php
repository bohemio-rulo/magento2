<?php
namespace Rulo\Offers\Model;

use Rulo\Offers\Api\Data\RegisterSaleProductInterface;

class RegisterSaleProduct extends \Magento\Framework\Model\AbstractModel implements RegisterSaleProductInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rulo\Offers\Model\ResourceModel\RegisterSaleProduct');
    }

    /**
     * @return int|mixed|null
     */
    public function getId()
    {
        return $this->_getData('entity_id');
    }

    /**
     * @param $value
     * @return RegisterSaleProduct
     */
    public function setId($value)
    {
        return $this->setData('entity_id', $value);
    }

    /**
     * @return mixed|string|null
     */
    public function getSku()
    {
        return $this->_getData(self::SKU);
    }

    /**
     * @param $sku
     * @return RegisterSaleProduct
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @return float|int|mixed|string|null
     */
    public function getQty()
    {
        return $this->_getData(self::QTY);
    }

    /**
     * @param $qty
     * @return RegisterSaleProduct
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }

    /**
     * @return mixed|string|null
     */
    public function getEmail()
    {
        return $this->_getData(self::EMAIL);
    }

    /**
     * @param $email
     * @return RegisterSaleProduct
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * @return mixed|string|null
     */
    public function getIncrementId()
    {
        return $this->_getData(self::INCREMENT_ID);
    }

    /**
     * @param $incrementId
     * @return RegisterSaleProduct
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * @return mixed|string|null
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * @param $createdAt
     * @return RegisterSaleProduct
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
