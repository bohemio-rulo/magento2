<?php
namespace Rulo\Offers\Model\ResourceModel;

class RegisterSaleProduct extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('register_sale_products', 'entity_id');
    }
}
?>
