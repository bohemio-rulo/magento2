<?php

namespace Rulo\Offers\Model\ResourceModel\RegisterSaleProduct;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rulo\Offers\Model\RegisterSaleProduct', 'Rulo\Offers\Model\ResourceModel\RegisterSaleProduct');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
