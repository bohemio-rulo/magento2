<?php

namespace Rulo\Offers\Model\Api;

use Rulo\Offers\Api\RegisterSaleProductManagementInterface;
use Rulo\Offers\Model\RegisterSaleProductFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Rulo\Offers\Api\Data\RegisterSaleProductInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class RegisterSaleProductManagement implements RegisterSaleProductManagementInterface
{
    /**
     * @var RegisterSaleProductFactory
     */
    protected $registerSaleProductFactory;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @param RegisterSaleProductFactory $registerSaleProductFactory
     * @param Json $json
     */
    public function __construct(
        RegisterSaleProductFactory $registerSaleProductFactory,
        Json $json
    ) {
        $this->registerSaleProductFactory = $registerSaleProductFactory;
        $this->json = $json;
    }

    /**
     * @param RegisterSaleProductInterface $registerSaleProduct
     * @return array[]
     * @throws \Exception
     */
    public function registerSale(RegisterSaleProductInterface $registerSaleProduct)
    {
        $result = [
            'error' => true,
            'Message' => __('Product was not properly registered')
        ];

        try {
            $registerSale = $this->registerSaleProductFactory->create();
            $registerSale->setData($registerSaleProduct->getData());

            if ($registerSale->save()) {
                $result = [
                    'success' => true,
                    'Message' => __('Product successfully registered')
                ];
            }
        } catch (NoSuchEntityException $e) {
            $result = [
                'error' => true,
                'Message' => $e->getMessage()
            ];
        }

        return [$result];
    }
}
