<?php

namespace Rulo\Offers\Model\Api;

use Rulo\Offers\Api\BestOfferManagementInterface;
use Rulo\Offers\Service\GetOffers;
use Magento\Framework\Serialize\Serializer\Json;

class BestOfferManagement implements BestOfferManagementInterface
{
    /**
     * @var GetOffers
     */
    protected $offers;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @param GetOffers $offers
     * @param Json $json
     */
    public function __construct(
        GetOffers $offers,
        Json $json
    ) {
        $this->offers = $offers;
        $this->json = $json;
    }

    /**
     * Return a Best Offer product.
     *
     * @param string $sku
     * @return bool|string
     */
    public function getBestOffer(string $sku)
    {
        $response = json_decode($this->offers->execute($sku), true);
        $offers = $response['offers'];
        $auxOffers = [];
        $qualification = [];
        $reviewsQuantity = [];

        foreach ($offers as $key => $offer) {
            $offersSelected[$offer['id']] = $offer;
            $auxOffers[$key] = $offer['seller'];
            $auxOffers[$key]['id'] = $offer['id'];
            $qualification[$key] = $offer['seller']['qualification'];
            $reviewsQuantity[$key] = $offer['seller']['reviews_quantity'];
        }

        array_multisort($qualification, SORT_DESC, $reviewsQuantity, SORT_DESC, $auxOffers);

        foreach ($auxOffers as $auxOffer) {
            if ($offersSelected[$auxOffer['id']]['stock'] > 0) {
                return ['offer' => $offersSelected[$auxOffer['id']]];
            }
        }

        return [
            [
                'error' => true,
                'message' => __('not found offers')
            ]
        ];
    }
}
