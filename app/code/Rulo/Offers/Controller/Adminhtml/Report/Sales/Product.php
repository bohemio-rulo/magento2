<?php

namespace Rulo\Offers\Controller\Adminhtml\Report\Sales;

class Product extends \Magento\Reports\Controller\Adminhtml\Report\Sales
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_showLastExecutionTime('report_product_aggregated', 'product');

        $this->_initAction()->_setActiveMenu(
            'Rulo_Offers::report_register_sale_product'
        )->_addBreadcrumb(
            __('Register Sale Product Report'),
            __('Register Sale ProductReport')
        );

        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Register Sale Product Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_sales_product.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
	}
}
