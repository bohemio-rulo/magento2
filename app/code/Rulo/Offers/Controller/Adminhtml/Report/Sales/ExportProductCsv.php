<?php

namespace Rulo\Offers\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportProductCsv extends \Magento\Reports\Controller\Adminhtml\Report\Sales
{
    /**
     * Export Register Sale Product report grid to CSV format
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        $fileName = 'register-sale-product.csv';
        $grid = $this->_view->getLayout()->createBlock(\Rulo\Offers\Block\Adminhtml\Sales\Product\Grid::class);
        $this->_initReportAction($grid);
        return $this->_fileFactory->create($fileName, $grid->getCsvFile(), DirectoryList::VAR_DIR);
    }
}
