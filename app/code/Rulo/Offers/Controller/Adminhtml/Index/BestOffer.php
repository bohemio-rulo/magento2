<?php

namespace Rulo\Offers\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;
class BestOffer extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $resultPage = $this->_resultPageFactory->create();
        $sku = $this->getRequest()->getParam('sku');

        $block = $resultPage->getLayout()
            ->createBlock(\Rulo\Offers\Block\Adminhtml\Index\BestOffer::class)
            ->setTemplate('Rulo_Offers::offer_index_best.phtml')
            ->setData('sku', $sku)
            ->toHtml();

        $result->setData(['output' => $block]);

        return $result;
	}
}
