<?php

declare(strict_types=1);

namespace Rulo\Offers\Service;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;

/**
 * Class GetOffers
 */
class GetOffers
{
    /**
     * API request URL
     */
    const API_REQUEST_URI = 'https://b80aa785-9d0f-4046-932d-ac78836b2d68.mock.pstmn.io/';

    /**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'getAllSkuOffers/:';

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var ClientFactory
     */
    private $clientFactory;

    /**
     * GitApiService constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Fetch some data from API
     */
    public function execute($sku)
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ];

        $repositoryName = 'magento/magento2';
        $response = $this->doRequest(static::API_REQUEST_ENDPOINT . $sku, $options);
        $status = $response->getStatusCode(); // 200 status code
        $responseBody = $response->getBody();
        $responseContent = $responseBody->getContents(); // here you will have the API response in JSON format

        return $responseContent;
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    ): Response {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => self::API_REQUEST_URI
        ]]);

        try {
            $response = $client->request(
                $requestMethod,
                self::API_REQUEST_URI . $uriEndpoint,
                $params
            );
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);

            $logger = $this->getLog();
            $logger->info(__('Error Conection POS: ENDPOINT: %1, %2', $uriEndpoint, $exception->getMessage()));
        }

        return $response;
    }

    /**
     * @param string $file
     * @return array \Zend\Log\Logger()
     */
    public function getLog($file = '/var/log/conection.log') {
        $writer = new \Zend\Log\Writer\Stream(BP . $file);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        return $logger;
    }
}
