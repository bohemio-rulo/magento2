<?php

namespace Rulo\Offers\Block\Adminhtml\Index;

class BestOffer extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var \Rulo\Offers\Service\GetOffers
     */
    protected $getOffers;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Rulo\Offers\Api\BestOfferManagementInterface $getOffers
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Rulo\Offers\Api\BestOfferManagementInterface $getOffers,
        array $data = []
    ) {
        $this->getOffers = $getOffers;

        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     * @throws \JsonException
     */
    public function getOffers($sku)
    {
        return $this->getOffers->getBestOffer($sku);
    }
}
