<?php

namespace Rulo\Offers\Block\Adminhtml\Sales\Product;

class Grid extends \Magento\Reports\Block\Adminhtml\Grid\AbstractGrid
{
    /**
     * GROUP BY criteria
     *
     * @var string
     */
    protected $_columnGroupBy = 'period';

    /**
     * @var \Rulo\Offers\Model\ResourceModel\Report\RegisterSaleProduct\Collection
     */
    protected $registerSaleProductCollection;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $resourceFactory
     * @param \Magento\Reports\Model\Grouped\CollectionFactory $collectionFactory
     * @param \Magento\Reports\Helper\Data $reportsData
     * @param \Rulo\Offers\Model\ResourceModel\Report\RegisterSaleProduct\Collection $registerSaleProductCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $resourceFactory,
        \Magento\Reports\Model\Grouped\CollectionFactory $collectionFactory,
        \Magento\Reports\Helper\Data $reportsData,
        \Rulo\Offers\Model\ResourceModel\Report\RegisterSaleProduct\CollectionFactory $registerSaleProductCollection,
        array $data = []
    ) {
        $this->registerSaleProductCollection = $registerSaleProductCollection;

        parent::__construct($context, $backendHelper, $resourceFactory, $collectionFactory, $reportsData, $data);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setCountTotals(false);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getResourceCollectionName()
    {
        return \Rulo\Offers\Model\ResourceModel\Report\RegisterSaleProduct\Collection::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'period',
            [
                'header' => __('Interval'),
                'index' => 'period',
                'sortable' => false,
                'period_type' => $this->getPeriodType(),
                'renderer' => \Magento\Reports\Block\Adminhtml\Sales\Grid\Column\Renderer\Date::class,
                'totals_label' => __('Total'),
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'type' => 'string',
                'sortable' => false,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'qty',
            [
                'header' => __('Order Quantity'),
                'index' => 'qty',
                'type' => 'number',
                'total' => 'sum',
                'sortable' => false,
                'header_css_class' => 'col-qty',
                'column_css_class' => 'col-qty'
            ]
        );

        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
                'type' => 'string',
                'sortable' => false,
                'header_css_class' => 'col-email',
                'column_css_class' => 'col-email'
            ]
        );

        $this->addColumn(
            'increment_id',
            [
                'header' => __('Order Id'),
                'index' => 'increment_id',
                'type' => 'string',
                'sortable' => false,
                'header_css_class' => 'col-increment-id',
                'column_css_class' => 'col-increment-id'
            ]
        );

        $this->addExportType('*/*/ExportProductCsv', __('CSV'));
        $this->addExportType('*/*/ExportProductExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * Apply sorting and filtering to collection
     *
     * @return $this|\Magento\Backend\Block\Widget\Grid
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _prepareCollection()
    {
        $filterData = $this->getFilterData();

        if ($filterData->getData('from') === null || $filterData->getData('to') === null) {
            $this->setCountTotals(false);
            $this->setCountSubTotals(false);
            return parent::_prepareCollection();
        }

        $periodFrom = $filterData->getData('from');
        $periodTo = $filterData->getData('to');

        if (!$periodFrom || !$periodTo) {
            return parent::_prepareCollection();
        }

        $storeIds = $this->_getStoreIds();

        $collection = $this->registerSaleProductCollection->create();
        $collection->setDateRange(
            $periodFrom, $periodTo
        )->setAggregatedColumns(
            $this->_getAggregatedColumns()
        )->addStoreFilter(
                $storeIds
        )->addProductFilter(
            $filterData->getData('sku_filter')
        );

        if ($this->_isExport) {
            $this->setCollection($collection);
            return $this;
        }

        $collection->load();
        $this->setCollection($collection);

        return $this;
    }
}
