<?php

namespace Rulo\Offers\Block\Adminhtml\Sales\Filter;

class Form extends \Magento\Reports\Block\Adminhtml\Filter\Form
{
    /**
     * Report type options
     *
     * @var array
     */
    protected $_reportTypeOptions = [];

    /**
     * Report field visibility
     *
     * @var array
     */
    protected $_fieldVisibility = [];

    /**
     * Report field options
     *
     * @var array
     */
    protected $_fieldOptions = [];

    /**
     * @return $this|Form
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        $form = $this->getForm();
        $htmlIdPrefix = $this->getForm()->getHtmlIdPrefix();
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof \Magento\Framework\Data\Form\Element\Fieldset) {
            $fieldset->addField(
                'sku_filter',
                'text',
                [
                    'name' => 'sku_filter',
                    'label' => __('Search Product by SKU'),
                    'title' => __('Search Product by SKU'),
                    'required' => true,
                    'css_class' => 'admin__field-small'
                ]
            );

            $dependences = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence');
            $atts = array('order_statuses', 'provider');
            foreach ($atts as $att) {
                if ($this->getFieldVisibility('show_' . $att) && $this->getFieldVisibility($att)) {
                    $dependences
                        ->addFieldMap($htmlIdPrefix . "show_" . $att, 'show_' . $att)
                        ->addFieldMap($htmlIdPrefix . $att, $att)
                        ->addFieldDependence($att, 'show_' . $att, '1');
                }
            }
            $this->setChild('form_after', $dependences);

            $form->setUseContainer(true);
            $this->setForm($form);
        }

        return $this;
    }
}
