require([
        'jquery',
        'domReady!',
        'slick'
    ],
    function($) {
        "use strict";
        $(document).ready(function($) {
            $('nav.navigation > ul').slick({
                loop: true,
                //arrows: false,
                slidesToShow: 5,
                //slidesToScroll: 4,
                dots: false,
                variableWidth: true
                //margin: 1,
                /*responsive: [{
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }]*/
            });
            /*$(window).on('resize', function() {
                $('.filter-options').slick('resize');
            });*/
        });
    });
